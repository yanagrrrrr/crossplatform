import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.HashSet;

public class Main {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        int x = 0;
        String s ="";

        while (!"4".equals(s)){
            System.out.println("1. To reevaluate products and print table PRESS 1");
            System.out.println("2. To delete products with entered name PRESS 2");
            System.out.println("3. To create Set of products produced this year PRESS 3");
            System.out.println("4. To EXIT PRESS 4");
            s = scan.next();

            try {
                x = Integer.parseInt(s);
            } catch (NumberFormatException e){
                System.out.println("Wrong input");
            }

            switch (x){
                case 1:
                    ArrayList<Product> productList = new ArrayList<Product>();
                    File file = new File("D:\\local_e\\nulp\\sem5\\KP\\kp2\\data.txt");

                    Product.FillListOfProducts(file, productList);

                    Product.PrintProductArray(productList);

                    Product.RevaluateProducts(productList);

                    System.out.println("\nAfter revaluation\n");
                    Product.PrintProductArray(productList);
                    Product.InitialiseMap(productList);
                    Product.PrintMapWithDates();

                    break;
                case 2:
                    System.out.println("Enter name of product to delete");
                    String nameToDelete = scan.next();
                    Product.DeleteByNameFromMap(nameToDelete);
                    break;
                case 3:
                    ArrayList<Product> productList1 = new ArrayList<Product>();
                    File file1 = new File("D:\\local_e\\nulp\\sem5\\KP\\kp2\\data1.txt");
                    Product.FillListOfProducts(file1, productList1);
                    ArrayList<Product> productList2 = new ArrayList<Product>();
                    File file2 = new File("D:\\local_e\\nulp\\sem5\\KP\\kp2\\data2.txt");
                    Product.FillListOfProducts(file2, productList2);

                    HashSet<Product>  setOfProducts = new HashSet<>();
                    setOfProducts = Product.DetectUniqueSet(productList1, productList2);
                    System.out.println("Unique set of products");

           Product.PrintSetOfProducts(setOfProducts);
         Product.DeleteProducedNotThisYear(setOfProducts);
                    System.out.println("\nProduced this year");
                    Product.PrintSetOfProducts(setOfProducts);
            }
        }
        System.out.println("Goodbye!");
    }
}
