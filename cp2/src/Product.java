import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

public class Product {

    public String name;
    public String dateOfManufacture;
    public String endDateOfConsumption;
    public double price;

    public static Map<String, ArrayList<String>> mapOfProducts = null;

    public Product(String Name, String startDate, String endDate, double Price) {
        name = Name;
        dateOfManufacture = startDate;
        endDateOfConsumption = endDate;
        price = Price;
        mapOfProducts = new TreeMap<String, ArrayList<String>>();

    }


    @Override
    public String toString() {
        return "Product\n" +
                "Name: '" + name + '\'' +
                ", Date of manufacture: '" + dateOfManufacture + '\'' +
                ", End date of consumption: '" + endDateOfConsumption + '\'' +
                ", Price :" + price +
                '₴';
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }

        Product product = (Product) obj;
        return (name.equals(product.name));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name== null) ? 0 : name.hashCode());
        return result;
    }


    public static void PrintProductArray(ArrayList<Product> pList){
        for (var p:
                pList) {
            System.out.println(p);
        }
    }

    public static void PrintSetOfProducts(HashSet<Product> productsSet){
        for(Product product : productsSet){
            System.out.println(product);
        }
    }

    public static void FillListOfProducts(File file, ArrayList<Product> pList ) {
        try {
            FileReader fr = new FileReader(file);
            BufferedReader reader = new BufferedReader(fr);
            String line = null;
            String[] splitted = null;
            while ((line = reader.readLine()) != null) {
                splitted = line.split(" ");

                Product pNew = new Product(splitted[0],
                        splitted[1],
                        splitted[2],
                        Double.valueOf(splitted[3]));

                pList.add(pNew);
            }
        } catch (Exception e) {
            System.out.println("Error! File not found");
        }
    }

    public static void RevaluateProducts(ArrayList<Product> pList){


        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        Calendar c = Calendar.getInstance();
        c.setTime(new Date()); // Now use today date.
        c.add(Calendar.DATE, 3); // Adding 3 days
        String todayDate = sdf.format(c.getTime());
        System.out.println(todayDate);

       for(var p:pList){
           if(p.endDateOfConsumption.equals(todayDate)){
                p.price = p.price *0.9;
           }
       }
    }

    public static void InitialiseMap(ArrayList<Product> products){

        for(Product product : products){
            ArrayList<String> productListInMap;
            if(!mapOfProducts.containsKey(product.endDateOfConsumption)){
                productListInMap= new ArrayList<String>();
                productListInMap.add(product.name);
                mapOfProducts.put(product.endDateOfConsumption, productListInMap);
            }else{
               mapOfProducts.get(product.endDateOfConsumption).add(product.name);
            }
        }

    }

    public static void PrintProductsNames(ArrayList<String> products) {
        for (String product : products){
            System.out.println("\t\t\t\t\t\t|\t" + product);
        }
    }

    public static void PrintMapWithDates(){

        System.out.println("End date of consumption\t\tProducts");
        for(Map.Entry<String, ArrayList<String>> entry : mapOfProducts.entrySet()){
            System.out.println(entry.getKey()+ "\t\t\t\t|");
            Product.PrintProductsNames(entry.getValue());
        }
    }

    public static void DeleteByNameFromMap(String nameToDelete){
        for(Map.Entry<String,ArrayList<String>> entry : mapOfProducts.entrySet()) {

            ListIterator<String> it = entry.getValue().listIterator();
            while(it.hasNext()){
                String str = it.next();
                if(str.equals(nameToDelete) ){
                   it.remove();
                }
            }
        }
        PrintMapWithDates();
    }


   public static HashSet<Product> DetectUniqueSet(ArrayList<Product> productList1,ArrayList<Product> productList2){
       HashSet<Product> setOfProducts = new HashSet<>();
       for(Product product : productList1){
        if(!setOfProducts.contains(product)){
                setOfProducts.add(product);
            }
       }
       for(Product product : productList2){
           if(!setOfProducts.contains(product)){
               setOfProducts.add(product);
           }else if(setOfProducts.contains(product)){
               setOfProducts.remove(product);
           }
       }

       return setOfProducts;
   }

   public static void DeleteProducedNotThisYear(HashSet<Product> setOfProducts){

       Iterator<Product> setIterator = setOfProducts.iterator();

       while (setIterator.hasNext()) {
           Product tmp = setIterator.next();

           String year = tmp.dateOfManufacture.split("\\.")[2];
           if (!year.equals("20")) {
             setIterator.remove();
           }
       }
   }

}
