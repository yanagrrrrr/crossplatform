import javax.swing.*;
import java.awt.*;
import java.util.Timer;
import java.util.TimerTask;
import java.awt.Color;
import javax.swing.JFrame;
import javax.swing.JColorChooser;

public class Monitor implements Runnable{
    private Thread thread;
    private Timer timer;

    public Monitor(Thread thread,Timer timer){
        this.thread=thread;
        this.timer=timer;
    }
    public void run(){

        JFrame frame = new JFrame("Info about thread '"+thread.getName());

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JTextArea label = new JTextArea("\nState:"+thread.getState()+" Thread priority:"+thread.getPriority()+" Is alive:"+thread.isAlive());
        JScrollPane sp = new JScrollPane(label);
        frame.getContentPane().add(sp);

        frame.setPreferredSize(new Dimension(600, 600));

        frame.pack();
        frame.setVisible(true);
        timer.schedule(new UpdateThreadInfo(thread,label),0,10);
    }
}
class UpdateThreadInfo extends TimerTask{
    private  Thread thread;
    private  JTextArea label;
   public UpdateThreadInfo(Thread thread,JTextArea label){
        this.thread=thread;
        this.label=label;
    }
   public void run(){
        String info="\nState:"+thread.getState()+" Thread priority:"+thread.getPriority()+" Is alive:"+thread.isAlive()+"";
        label.append(info);
    }
}


