import java.awt.*;

public class TShirt extends Clothing{

    String Size;

    TShirt(){
        Brand = "Nike";
        Colour = "White";
        Price = 10.99;
        CottonPercent = 90;
        Size = "XS";
    }

    public TShirt(String brand, double price, int cottonPercent, String colour, String size) {
        super(brand, price, cottonPercent, colour);
        Size = size;
    }

    @Override
    public String toString() {
        return "\nT-shirt colour: " +Colour + "\nBrand: " + Brand + "\nPrice: "
                + Price + "$\nCotton percentage: "+ CottonPercent +"%\nSize: " + Size ;
    }
}
