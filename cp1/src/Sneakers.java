


public class Sneakers extends Clothing {

    int Size;

    Sneakers(){
        Brand = "Mizuno";
        Price = 50.49;
        CottonPercent = 50;
        Colour = "black";
        Size = 36;
    }

    Sneakers(String brand, double price, int cottonPercent, String colour, int size){
    super(brand, price, cottonPercent, colour);
    Size = size;
    }

    @Override
    public String toString(){
        return "\nSneackers colour: " +Colour + "\nBrand: " + Brand + "\nPrice: "
                + Price + "$\nCotton percentage: "+ CottonPercent +"%\nSize: "+ Size;
    }
}
