import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        ArrayList<Clothing> clothingArray = new ArrayList<>();
        clothingArray.add(new TShirt("Adidas", 12.49, 87, "yellow", "s"));
        clothingArray.add(new TShirt("Puma", 12.49, 94, "yellow", "xl"));
        clothingArray.add(new TShirt("Tommу Hilfiger", 12.49, 60, "yellow", "m"));
        clothingArray.add(new TShirt("Asics", 25.79, 84, "yellow", "l"));
        clothingArray.add(new Sneakers("Asics",60.25, 70, "purple" ,40));
        clothingArray.add(new SportPants());

    SortingCottonManager sortManager = new SortingCottonManager();
    sortManager.printArray(clothingArray);

    sortManager.descendingSort(clothingArray);

    System.out.println("\nDescending sort : ");
    sortManager.printArray(clothingArray);
    System.out.println("\nAscending sort : ");
    sortManager.ascendingSort(clothingArray);
    sortManager.printArray(clothingArray);

    SortingCottonManager.innerSortingClass inSortClass = sortManager.new innerSortingClass();

        inSortClass.descendingSortByInnerClass(clothingArray);
        System.out.println("\nHere we go again");
        sortManager.printArray(clothingArray);


        clothingArray.add(new Sneakers());
        clothingArray.add(new Sneakers("Puma", 60.25, 49, "green", 39));
        clothingArray.add(new Sneakers("Asics", 80, 69, "multicolour", 44));

        sortManager.anonymous.sort(clothingArray);
        System.out.println("\nSNEAKERS");
        sortManager.printArray(clothingArray);



        sortManager.iSort.sort(clothingArray);
        System.out.println("\nLAMBDA");
        sortManager.printArray(clothingArray);


        Scanner in = new Scanner(System.in);
        System.out.println("Enter brand to search:");

        String brand = in.nextLine();


        System.out.println("\nRESULT OF SEARCHING BY BRAND "+ brand );
        SearchManager.searchByBrand(brand, clothingArray);

        System.out.println("Enter price to search:");
        double price = in.nextDouble();
        System.out.println("\nRESULT OF SEARCHING BY PRICE "+price);
        SearchManager.searchByPrice(price, clothingArray);




    }
}
