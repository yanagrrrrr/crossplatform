
/*
Реалізувати ієрархію класів що представляють спортивний одяг (кросівки, спортивний костюм та ін)
Реалізувати пошук одягу за брендом та за ціною (два окремі методи)
Реалізувати сортування одягу за його % бавовни в його складі
Реалізація сортування має передбачати можливість сортувати як за спаданням, так і за зростанням

 */

public class Clothing {

    String Brand;
    double Price;
    int CottonPercent;
    String Colour;

    Clothing(){ }

    Clothing(String brand, double price, int cottonPercent, String colour){
        Brand = brand;
        Price = price ;
        CottonPercent = cottonPercent;
        Colour = colour;
    }
}
