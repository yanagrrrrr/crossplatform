import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import  java.util.Comparator;

public class SortingCottonManager {

    SortingCottonManager.comparatorStatic staticInnerClass = new SortingCottonManager.comparatorStatic();

    SortingCottonManager(){}

//inner static class
   public static class comparatorStatic implements Comparator<Clothing> {

       @Override
       public int compare(Clothing c1, Clothing c2) {
           if (c1.CottonPercent == c2.CottonPercent) {
               return 0;
           } else if (c1.CottonPercent > c2.CottonPercent) {
               return 1;
           } else {
               return -1;
           }
       }
   }

    public void ascendingSort(ArrayList<Clothing> list){
       list.sort(staticInnerClass);
    }

    public void descendingSort(ArrayList<Clothing> list){
       list.sort(staticInnerClass.reversed());
    }

    public void printArray(ArrayList<Clothing> clothes){
        for(Clothing clothe : clothes){
            System.out.println(clothe);
        }
    }


        //inner class
    class innerSortingClass implements Comparator<Clothing>{
        public innerSortingClass(){}
        @Override
        public int compare(Clothing c1, Clothing c2) {
            if(c1.CottonPercent == c2.CottonPercent) {
                return 0;
            }else if(c1.CottonPercent > c2.CottonPercent){
                return 1;
            }else{
                return  -1;
            }

        }

        public void ascendingSortByInnerClass(ArrayList<Clothing> list){
            SortingCottonManager.innerSortingClass innerClass = new SortingCottonManager.innerSortingClass();
            list.sort(innerClass);
        }

        public void descendingSortByInnerClass(ArrayList<Clothing> l){
            SortingCottonManager.innerSortingClass innerClass = new SortingCottonManager.innerSortingClass();
            l.sort(innerClass.reversed());
        }
    }

//anonymous inner class
    iSorting anonymous = new iSorting(){
        @Override
        public void sort(ArrayList<Clothing> l){
            l.sort(staticInnerClass);
        }
    };

    //lambda-function
    iSorting iSort = (arrList) -> arrList.sort(staticInnerClass);

}
