


public class SportPants extends Clothing {
    int Length;

    SportPants(){
        Brand = "Reebok";
        Colour = "Blue";
        Price = 29.99;
        CottonPercent =75;
        Length = 90;
    }

    public SportPants(String brand, double price, int cottonPercent, String colour, int length) {
        super(brand, price, cottonPercent, colour);
        Length = length;
    }

    @Override
    public String toString() {
        return "\nSport pants colour: " +Colour + "\nBrand: " + Brand + "\nPrice: "
                + Price + "$\nCotton percentage: "+ CottonPercent +"%\nLength: " + Length+" cm" ;
    }
}
