import java.util.ArrayList;

public  class SearchManager {

    public static void searchByBrand(String brandName, ArrayList<Clothing> clothes) {
        int counter = 0;
        for (Clothing clothe : clothes) {
            if (brandName.equals(clothe.Brand)) {
                System.out.println(clothe);
            counter++;
            }
        }
        if(counter == 0){
            System.out.println("There is no clothing with the brand "+brandName+"\n");
        }
    }
        public static void searchByPrice(double price, ArrayList<Clothing> clothes){
        int counter = 0;

            for (Clothing clothe : clothes) {
                if (price == clothe.Price) {
                    System.out.println(clothe);
                    counter++;
                }
            }

            if(counter == 0){
                System.out.println("There is no clothing with the price "+price+"$\n");
            }
        }

    }

