
import java.util.ArrayList;

@FunctionalInterface
public interface iSorting {
    public void sort(ArrayList<Clothing> arr) ;
}
