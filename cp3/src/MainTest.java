import org.junit.Assert;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {

    @org.junit.jupiter.api.Test
    void toUpperCaseLastChar() {
        String testString = "Hello it is me";

        String expected = "HellO iT iS mE";

        String actual = Main.toUpperCaseLastChar(testString);

        Assert.assertEquals(expected, actual);
    }
}