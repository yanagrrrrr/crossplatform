

/*Варіант 8. Знайти всі дати, задані різними поширеними форматами.
             Визначити, в якому діапазоні вони знаходяться.
             Замінити всі дати на середнє число діапазону.
             Речення, в яких були знайдені дати, вивести великими літерами*/

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Main {

    public static void main(String[] args)  throws Exception {

        TreeMap<myDate, ArrayList<Integer>> dateIndex = new TreeMap<myDate, ArrayList<Integer>>(
                new Comparator<myDate>() {
                    @Override
                    public int compare(myDate d1, myDate d2) {
                        SimpleDateFormat sdf = new SimpleDateFormat("dd"+d1.separator+ "MM"+ d1.separator+ "yyyy");
                        d2.separator = d1.separator;
                        try {
                            Date date1 = sdf.parse(d1.toString());
                            Date date2 = sdf.parse(d2.toString());
                            if (date1.compareTo(date2) > 0) {
                                return 1;
                            } else if (date1.compareTo(date2) < 0) {
                                return -1;
                            } else if (date1.compareTo(date2) == 0) {
                                return 0;
                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        return 0;
                    }
                }
        );


        String everything = myDate.ReadText();
        System.out.println("Text from file:\n"+everything);

        myDate tmpDate = null;
        everything = everything.replaceAll("\r", "").replaceAll("\n","");
        String [] splitted = everything.split(" ");

        System.out.println("Found dates:\n");
        for(int i =0; i< splitted.length; ++i) {
            tmpDate = myDate.FindDate(splitted[i]);
            if (tmpDate != null) {
                if (!dateIndex.containsKey(tmpDate)) {
                    ArrayList<Integer> indexes = new ArrayList<>();
                    indexes.add(i);
                    dateIndex.put(tmpDate, indexes);
                }else{
                    dateIndex.get(tmpDate).add(i);
                }
                System.out.println(tmpDate);
            }
        }

        myDate minimalDate = dateIndex.firstKey();
        myDate maximalDate = dateIndex.lastKey();
        System.out.println("Range of dates from " +minimalDate.toString()+" to " + maximalDate.toString());
        myDate avgDate =  myDate.FindAvgDate(minimalDate, maximalDate);
        System.out.println("Average date: "+avgDate);

        System.out.println("\nText with replaced dates:");
        myDate.ReplaceAvg(splitted, dateIndex, avgDate);
        System.out.println(myDate.MakeText(splitted));

        System.out.println("\nText with upper case sentences, which contains dates:");
        String result = myDate.ToUpperCaseIfContainsDate(splitted);
        System.out.println(result);


        //Замінити всі останні букви слів тексту на великі


        String testString = "hello world. this is me";

        Scanner in = new Scanner(System.in);
        System.out.print("Enter text: ");
        testString = in.nextLine();

        System.out.println(testString);
    testString = Main.toUpperCaseLastChar(testString);

        System.out.println(testString);


    }

public  static String toUpperCaseLastChar(String string){
    Pattern pattern = Pattern.compile("([a-zа-яіїґє']+)");
    Matcher matcher = pattern.matcher(string);
    char[] str  = string.toCharArray();
    while (matcher.find()) {

        str[matcher.end()-1] = Character.toUpperCase(str[matcher.end()-1]);
    }
    return  String.valueOf(str);

}


}

