

/*Варіант 8. Знайти всі дати, задані різними поширеними форматами.
             Визначити, в якому діапазоні вони знаходяться.
             Замінити всі дати на середнє число діапазону.
             Речення, в яких були знайдені дати, вивести великими літерами*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.lang.reflect.Array;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.temporal.ChronoUnit;
import java.util.*;

public  class myDate implements Comparator<myDate>  {
int day;
int month;
int year;
char separator;

public myDate(){}

    @Override
    public int compare(myDate d1, myDate d2) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd"+d1.separator+ "MM"+ d1.separator+ "yyyy");
        d2.separator = d1.separator;
        try {
            Date date1 = sdf.parse(d1.toString());
            Date date2 = sdf.parse(d2.toString());
            if (date1.compareTo(date2) > 0) {
               return 1;
            } else if (date1.compareTo(date2) < 0) {
               return -1;
            } else if (date1.compareTo(date2) == 0) {
                return 0;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }



    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        myDate date = (myDate) obj;
        return (date.day == ((myDate) obj).day &&
                date.month == ((myDate) obj).month&&
                date.year == ((myDate) obj).year&&
                date.separator == ((myDate) obj).separator);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + day + month+year;
        return result;
    }

    @Override
    public String toString() {
        return String.valueOf(day) + String.valueOf(separator) +
                String.valueOf(month) + String.valueOf(separator) +
                String.valueOf(year);
    }



    public myDate (int Day,int Month, int Year,char Separator ){
            day = Day;
            month = Month;
            year = Year;
            separator = Separator;
}

    public static myDate FindDate(String text){
        if(text.isBlank()){return null;}
        int day = 0;
        int month = 0;
        int year = 0;
        String separator = "\\.";

        //if(text.length()!=10){return null;}

    for(int i = 0;i<text.length();++i){
        if(Character.isLetter(text.charAt(i)) || Character.isSpaceChar(text.charAt(i))){
            return null;
        }else{
            if(Character.isDigit(text.charAt(i))){
                continue;
            }
        }
         if(isSeparator(text.charAt(i))){
             separator = myDate.ReturnSeparator(text.charAt(i));
             break;
        }
    }
        String[] dateArray = text.split(separator);
        day = Integer.valueOf(dateArray[0]);
        month = Integer.valueOf(dateArray[1]);
        year = Integer.valueOf(dateArray[2]);
        char c = ' ';

        if(isDot(separator)){
            c = '.';
        }else{
            c = separator.charAt(0);
        }
        myDate result = new myDate(day,month,year,c);
        if(isValidDate(result)){
            return result;}
        return null;
    }



    public  static boolean isSeparator(char c){
    if(c == '.'||c=='-' || c=='/'){
        return true;
    }
    else {
        return false; }
    }
    public static String ReturnSeparator(char c){
    if(c == '/'){
        return "/";
    }else if(c == '-'){
        return "-";
    }else if(c == '.'){
        return "\\.";
    }else{return null;}
        }

    public static boolean isDot(String sep){
        if(sep == "\\."){return true;}
        else{return false;}
        }
    public static boolean isValidDate(myDate date){
        if(date.year > 0 && date.year<3000){
            if(date.month >0 && date.month<13){
                if(date.day > 0 &&date.day<32){
                    return true;
                }
            }
        }
        return false;
    }

    public static myDate FindAvgDate(myDate d1, myDate d2) {

        SimpleDateFormat sdf = new SimpleDateFormat("dd" + d1.separator + "MM" + d1.separator + "yyyy");
        d2.separator = d1.separator;
        try {
            Date date1 = sdf.parse(d1.toString());
            Date date2 = sdf.parse(d2.toString());
            long milisec = date1.getTime() + date2.getTime();
            milisec/=2;
            var middate = new Date(milisec);

            Calendar c = Calendar.getInstance();
            c.setTime(middate);

            int day =c.get(Calendar.DAY_OF_MONTH);
            int month = c.get(Calendar.MONTH)+1;
            int year = c.get(Calendar.YEAR);
            myDate result = new myDate(day, month, year,d1.separator);
            return result;

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void ReplaceAvg(String[] text, TreeMap<myDate, ArrayList<Integer>> dates, myDate avg){

        for(Map.Entry<myDate, ArrayList<Integer>> entry : dates.entrySet()){
            for(int index : entry.getValue()){
                text[index] = avg.toString();
            }

        }
    }

    public static String ToUpperCaseIfContainsDate(String[] splitted){
        myDate tmpDate = new myDate();
        StringBuffer strBuffer = new StringBuffer();
        for(int i = 0;i<splitted.length;++i){
            strBuffer.append(splitted[i]+" ");
        }

        String[] sentences = strBuffer.toString().split("(\\?|!)");
        for(int i = 0; i<sentences.length;++i){
            String[] tmp = sentences[i].split(" ");
            for(int j = 0;j<tmp.length;++j){
                tmpDate = myDate.FindDate(tmp[j]);
                if(tmpDate != null){
                    sentences[i] = sentences[i].toUpperCase();
                    break;
                }
            }
        }
        strBuffer.delete(0, strBuffer.length());
        for(String sentence:sentences){
            strBuffer.append(sentence+". ");
        }
        return strBuffer.toString();
    }

    public static String ReadText(){
    try {
        BufferedReader br = new BufferedReader(new FileReader("data.txt"));
        StringBuilder sb = new StringBuilder();
        String line = br.readLine();
        while (line != null) {
            sb.append(line);
            sb.append(System.lineSeparator());
            line = br.readLine();
        }
        String everything = sb.toString();
        return everything;
    }catch (Exception e){
            System.out.println("Error! File not found");}
    return null;
    };

    public static String MakeText(String[] splitted){
        StringBuffer strBuffer = new StringBuffer();
        for(int i = 0;i<splitted.length;++i){
            strBuffer.append(splitted[i]+" ");
        }
        return strBuffer.toString();
    }


}









